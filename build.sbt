import Dependencies._

ThisBuild / scalaVersion := "2.12.12"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memobase"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}
lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Record Deleter",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties"  => MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case "log4j2.xml"        => MergeStrategy.first
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.memobase.App"),
    resolvers ++= Seq(
      "Memobase Utils" at "https://dl.bintray.com/memoriav/memobase"
    ),
    libraryDependencies ++= Seq(
      elastic4s,
      kafkaClients,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      mariadbJavaClient,
      upickle,
      scalaTest % Test
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
