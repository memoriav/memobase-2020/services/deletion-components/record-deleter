/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import org.apache.logging.log4j.scala.Logging

import java.sql.{Connection, DriverManager}
import scala.util.{Failure, Success, Try}

trait MariadbClient {
  self: Logging with AppSettings =>

  private type DeletionResult =
    Try[(Option[String], Boolean, Boolean, Boolean, Boolean)]

  private val noRows = Success((None, false, false, false, false))

  private var connection = getConnection

  private def getConnection: Connection = {
    val c =
      DriverManager.getConnection(mariaDBHost, mariaDBUser, mariaDBPassword)
    c.setAutoCommit(false)
    c
  }

  private def isLocalFile(uri: String): Boolean = uri.startsWith("file:")

  private def generateDeletionResultMessage(
                                             resourceName: String,
                                             entitiesTable: Boolean,
                                             metadataTable: Boolean,
                                             manifestV2Table: Boolean,
                                             manifestV3Table: Boolean
                                           ): String = {
    s"$resourceName entry deleted in tables " +
      s"${
        if (entitiesTable) {
          "entities, "
        }
        else {
          ""
        }
      }" +
      s"${
        if (metadataTable) {
          "metadata, "
        }
        else {
          ""
        }
      }" +
      s"${
        if (manifestV2Table) {
          "iiif_manifests_v2, "
        }
        else {
          ""
        }
      }" +
      s"${
        if (manifestV3Table) {
          "iiif_manifests_v3"
        }
        else {
          ""
        }
      }"
  }

  private def updateDigitalObjectResult(
                                         record: ProcessedRecord,
                                         deletionResult: DeletionResult
                                       ): ProcessedRecord = {
    deletionResult match {
      case Success((None, false, false, false, false)) =>
        record.setDigitalObjectStatus(
          ProcessingIgnore,
          s"Digital object not found in media DB"
        )
      case Success((uri, entities, metadata, manifestV2, manifestV3)) =>
        (if (uri.isDefined && isLocalFile(uri.get)) {
          record.addBinary(uri.get)
        } else {
          record
        }).setDigitalObjectStatus(
          ProcessingSuccess,
          generateDeletionResultMessage(
            "Digital object",
            entities,
            metadata,
            manifestV2,
            manifestV3
          )
        )
      case Failure(ex) =>
        record.setDigitalObjectStatus(
          ProcessingFatal,
          s"Digital object media DB entries deletion failed${if (record.dryRun) " (dry-run mode)" else ""}: ${ex.getMessage}"
        )
    }

  }

  //noinspection ScalaStyle
  private def updateThumbnailResult(
                                     record: ProcessedRecord,
                                     posterDeletionResult: DeletionResult,
                                     snippetDeletionResult: DeletionResult
                                   ): ProcessedRecord = {
    (posterDeletionResult, snippetDeletionResult) match {
      case (Failure(e), _) =>
        // Something went wrong
        record.setThumbnailStatus(
          ProcessingFatal,
          s"Poster entries deletion failed${if (record.dryRun) " (dry-run mode)" else ""}: ${e.getMessage}"
        )
      case (_, Failure(e)) =>
        // Something went wrong
        record.setThumbnailStatus(
          ProcessingFatal,
          s"Snippet entries deletion failed${if (record.dryRun) " (dry-run mode)" else ""}: ${e.getMessage}"
        )
      case (`noRows`, `noRows`) =>
        record.setThumbnailStatus(ProcessingIgnore, "No thumbnails found")
      case (
        Success((uri, entities, metadata, manifestV2, manifestV3)),
        `noRows`
        ) =>
        (if (uri.isDefined && isLocalFile(uri.get)) {
          record.addBinary(uri.get)
        } else {
          logger.warn("Poster entry does not point to a local file!")
          record
        }).setThumbnailStatus(
          ProcessingSuccess,
          generateDeletionResultMessage(
            "Poster",
            entities,
            metadata,
            manifestV2,
            manifestV3
          )
        )
      case (
        `noRows`,
        Success((uri, entities, metadata, manifestV2, manifestV3))
        ) =>
        (if (uri.isDefined && isLocalFile(uri.get)) {
          record.addBinary(uri.get)
        } else {
          logger.warn("Snippet entry does not point to a local file!")
          record
        }).setThumbnailStatus(
          ProcessingSuccess,
          generateDeletionResultMessage(
            "Snippet",
            entities,
            metadata,
            manifestV2,
            manifestV3
          )
        )
      case (_, _) =>
        record.setThumbnailStatus(
          ProcessingWarning,
          "Snippets and posters found. This should not happen!"
        )
    }
  }

  /**
    * Updates `ProcessedRecord` instance if digital objects and/or thumbnails were identified
    * in MariaDB table. The following cases are possible:
    * * Digital object with local binary and thumbnail present
    * * Digital object with remote binary and thumbnail present
    * * Only a digital object with local binary present
    * * Only a digital object with remote binary present
    * * Only a thumbnail present
    *
    * @param record `ProcessedRecord` instance
    * @return Updated `ProcesedRecord` instance
    */
  //noinspection ScalaStyle
  def deleteMariaDBEntries(record: ProcessedRecord): ProcessedRecord = {
    val digitalObjectId = record.recordId + "-1"
    val digitalObjectDeletionResult =
      deleteRows(digitalObjectId)
    val posterDeletionResult =
      deleteRows(s"$digitalObjectId-poster")
    val introDeletionResult =
      deleteRows(s"$digitalObjectId-intro")
    (if (record.dryRun) {
      Try(connection.rollback())
    } else {
      Try(connection.commit())
    }) match {
      case Success(_) =>
        logger.info(s"MariaDB entries for record ${record.recordId} successfully deleted${if (record.dryRun) " (dry-run mode)" else ""}.")
        val updatedRecord = updateDigitalObjectResult(
          record,
          digitalObjectDeletionResult
        )
        updateThumbnailResult(
          updatedRecord,
          posterDeletionResult,
          introDeletionResult
        )
      case Failure(ex) =>
        logger.warn(s"Deletion of MariaDB entries for record ${record.recordId} failed${if (record.dryRun) " (dry-run mode)" else ""}: ${ex.getMessage}.")
        val updatedRecord = updateDigitalObjectResult(
          record,
          Failure(ex)
        )
        updateThumbnailResult(
          updatedRecord,
          Failure(ex),
          Failure(ex)
        )
    }
  }

  def checkDBConnection(timeout: Int): Unit = {
    if (connection == null || !connection.isValid(timeout)) {
      logger.info("DB connection is non-existent or invalid.")
      if (!connection.isClosed) {
        connection.close()
      }
      connection = getConnection
    }
  }

  def dbConnectorClose(): Unit = {
    logger.debug("Closing connection to database")
    connection.close()
  }

  /**
    * Deletes related rows
    *
    * @param id Record's id
    * @return An optional media file URI and success status for each row
    */
  private def deleteRows(
                          id: String
                        ): DeletionResult =
    Try {
      val stmt = connection.createStatement
      val deleteEntities =
        s"DELETE FROM entities WHERE sig = '$id' RETURNING uri"
      val resultSet = stmt.executeQuery(deleteEntities)
      val (entitiesDeleted, uri) = if (resultSet.next) {
        val u = Try(resultSet.getString("uri")).toOption
        if (u.isEmpty) {
          logger.warn(s"No uri found in table entities for record $id-1")
        }
        (true, u)
      } else {
        (false, None)
      }
      val delete: String => Boolean = tableName => {
        val deleteQuery = s"DELETE FROM $tableName WHERE sig = '$id'"
        val resultSet = stmt.executeQuery(deleteQuery)
        resultSet.next
      }
      (
        uri,
        entitiesDeleted,
        delete("metadata"),
        delete("iiif_manifests_v2"),
        delete("iiif_manifests_v3")
      )
    }
}
