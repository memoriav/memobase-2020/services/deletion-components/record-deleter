/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import org.apache.kafka.clients.producer.{
  KafkaProducer,
  ProducerRecord,
  RecordMetadata
}
import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.scala.Logging

import java.util.concurrent.{Future => JavaFuture}

abstract class Reporter {
  self: AppSettings with Logging =>

  private lazy val producer = new KafkaProducer[String, String](producerProps)

  def reportResult(record: ProcessedRecord): Unit = {
    val id = s"https://memobase.ch/record/${record.recordId}"
    val results = List(
      record.processResults.record.get,
      record.processResults.digitalObject.get,
      record.processResults.thumbnail.get,
      record.processResults.binaries.get
    )
    val status: ProcessingStatus = results
      .map(_._1)
      .reduceLeft((agg, x) =>
        (agg, x) match {
          case (ProcessingFatal, _)   => ProcessingFatal
          case (_, ProcessingFatal)   => ProcessingFatal
          case (ProcessingWarning, _) => ProcessingWarning
          case (_, ProcessingWarning) => ProcessingWarning
          case (ProcessingSuccess, _) => ProcessingSuccess
          case (_, x)                 => x
        }
      )
    val message = results
      .map(_._2)
      .zip(List("RECORD: ", "DIGITAL OBJECT: ", "THUMBNAIL: ", "BINARIES: "))
      .map(x => x._2 + x._1)
      .mkString(" -- ")
    send(id, status, message, record.headers)
  }

  private def send(
      id: String,
      status: ProcessingStatus,
      msg: String,
      headers: Headers
  ): JavaFuture[RecordMetadata] = {
    val report = Report(id, status, msg, groupId)
    logger.debug(s"Sending report for record $id with status $status: $msg")
    producer.send(
      new ProducerRecord(reportingTopic, null, id, report.toString, headers)
    )
  }

  def closeReporter(): Unit = {
    logger.debug("Closing Kafka producer instance")
    producer.close()
  }
}
