/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{
  ProcessedRecord,
  ProcessingFatal,
  ProcessingIgnore,
  ProcessingSuccess
}
import org.apache.logging.log4j.scala.Logging

import java.io.File

/**
  * Handles the deletion of binaries
  */
trait BinaryHandler {
  self: Logging with AppSettings =>

  def deleteBinaries(record: ProcessedRecord): ProcessedRecord = {
    if (record.binaries.nonEmpty) {
      val result = record.binaries
        .map(_.replaceFirst("file://", ""))
        .map(fileName => (new File(fileName), fileName))
        .map(file =>
          (if (record.dryRun) file._1.exists else file._1.delete, file._2)
        )
        .foldLeft[List[String]](List())((errors, x) =>
          if (!x._1) {
            logger.warn(s"Deletion of file ${x._2} failed${if (record.dryRun) " (dry-run mode)" else ""}!")
            errors :+ s"${x._2} deletion failed"
          } else {
            errors
          }
        )
      if (result.isEmpty) {
        logger.info(
          s"Deletion of binaries for record ${record.recordId} successful${if (record.dryRun) " (dry-run mode)" else ""}"
        )
        record.setBinariesStatus(
          ProcessingSuccess,
          "Binaries deletion successful"
        )
      } else {
        logger.warn(
          s"Deletion of binaries for record ${record.recordId} failed${if (record.dryRun) " (dry-run mode)" else ""}"
        )
        record.setBinariesStatus(ProcessingFatal, result.mkString("; "))
      }
    } else {
      record.setBinariesStatus(ProcessingIgnore, "No attached local binaries")
    }
  }

  def deleteCachedBinaries(record: ProcessedRecord): ProcessedRecord = {
    if (record.binaries.nonEmpty) {
      val result = record.binaries
        .map(_.replaceFirst("file://", ""))
        .map(_.replaceFirst(mediaFolderRootPath, mediaFolderCacheRootPath))
        .map(fileName => (new File(fileName), fileName))
        .map(file =>
          (if (record.dryRun) file._1.exists else file._1.delete, file._2)
        )
        .foldLeft[List[String]](List())((errors, x) =>
          if (!x._1) {
            logger.info(s"No thumbnail ${x._2} exists${if (record.dryRun) " (dry-run mode)" else ""}!")
            errors :+ s"Deletion of thumbnail ${x._2} failed (probably because there are no cached thumbnails...)"
          } else {
            errors
          }
        )
      if (result.isEmpty) {
        logger.info(
          s"Deletion of cached thumbnails for record ${record.recordId} successful${if (record.dryRun) " (dry-run mode)" else ""}"
        )
        record.setBinariesStatus(
          ProcessingSuccess,
          "Cached thumbnails deletion successful"
        )
      } else {
        logger.info(
          s"Thumbnail ${record.recordId} not available${if (record.dryRun) " (dry-run mode)" else ""}"
        )
        record.setBinariesStatus(ProcessingIgnore, result.mkString("; "))
      }
    } else {
      record.setBinariesStatus(ProcessingIgnore, "No attached local binaries")
    }
  }
}
