/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.util.Properties

trait AppSettings {
  val groupId: String = sys.env("GROUP_ID")
  val producerProps: Properties = {
    val props = new Properties()
    props.put("bootstrap.servers", sys.env("KAFKA_BOOTSTRAP_SERVERS"))
    props.put(
      "key.serializer",
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    props.put(
      "value.serializer",
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    props.put("batch.size", "131072")
    props.put("compression.type", "gzip")
    props
  }
  val consumerProps: Properties = {
    val props = new Properties()
    props.put("bootstrap.servers", sys.env("KAFKA_BOOTSTRAP_SERVERS"))
    props.put(
      "key.deserializer",
      "org.apache.kafka.common.serialization.StringDeserializer"
    )
    props.put(
      "value.deserializer",
      "org.apache.kafka.common.serialization.StringDeserializer"
    )
    props.put("fetch.min.bytes", "1048576")
    props.put("group.id", groupId)
    props.put("allow.auto.create.topics", "false")
    props.put("auto.offset.reset", "earliest")
    props.put("max.poll.records", "5000")
    props
  }
  val inputTopic: String = sys.env("TOPIC_IN")
  val reportingTopic: String = sys.env("TOPIC_PROCESS")
  val esHost: String = {
    val host = sys.env("ELASTIC_HOST")
    val scheme = if (host.startsWith("http://")) { "" }
    else { "http://" }
    s"$scheme$host:${sys.env("ELASTIC_PORT")}"
  }
  val esFrontendIndex: String = sys.env("ELASTIC_FRONTEND_INDEX")
  val esApiIndex: String = sys.env("ELASTIC_API_INDEX")
  val mariaDBHost: String =
    s"jdbc:mariadb://${sys.env("MARIADB_HOST")}:${sys.env("MARIADB_PORT")}/${sys.env("MARIADB_DATABASE")}"
  val mariaDBPassword: String =
    sys.env("MARIADB_PASSWORD").replaceAll("\\s+$", "")
  val mariaDBTables: List[String] = sys
    .env("MARIADB_TABLES")
    .split(", *")
    .toList
  val mariaDBUser: String = sys.env("MARIADB_USER").replaceAll("\\s+$", "")
  val mediaFolderRootPath: String = sys.env("MEDIA_FOLDER_ROOT_PATH")
  val mediaFolderCacheRootPath: String = sys.env("MEDIA_FOLDER_CACHE_ROOT_PATH")

}
