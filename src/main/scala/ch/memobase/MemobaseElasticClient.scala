/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{
  ProcessedRecord,
  ProcessingFatal,
  ProcessingIgnore,
  ProcessingSuccess
}
import com.sksamuel.elastic4s
import com.sksamuel.elastic4s.ElasticApi.deleteById
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.{
  ElasticClient,
  ElasticProperties,
  RequestFailure,
  RequestSuccess
}
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.ExecutionContext.Implicits.global

trait MemobaseElasticClient {
  self: Logging with AppSettings =>

  protected val client: elastic4s.ElasticClient = {
    val props = ElasticProperties(esHost)
    ElasticClient(JavaClient(props))
  }

  def existsInES(record: ProcessedRecord, esIndex: String, fullyQualifiedId: Boolean = false): ProcessedRecord = {
    val recordId = if (fullyQualifiedId) {s"https://memobase.ch/record/${record.recordId}"} else {record.recordId}
    client
      .execute {
        logger.info(
          s"Deleting $recordId in Elasticsearch index $esIndex"
        )
        exists(recordId, esIndex)
      }
      .map {
        case success: RequestSuccess[Boolean] if success.result =>
          logger.debug(
            s"Record $recordId exists in Elasticsearch index $esIndex"
          )
          record.setRecordStatus(
            ProcessingSuccess,
            "Record exists in Elasticsearch"
          )
        case _: RequestSuccess[_] =>
          logger.debug(
            s"Record $recordId does not exist in Elasticsearch index $esIndex"
          )
          record.setRecordStatus(
            ProcessingIgnore,
            "Record does not exist in Elasticearch index $esIndex"
          )

        case failure: RequestFailure =>
          logger.warn(
            s"Check of existence for record $recordId in Elasticsearch index $esIndex failed: ${failure.error.reason}"
          )
          record.setRecordStatus(
            ProcessingFatal,
            s"Check for existence in Elasticsearch index $esIndex failed: ${failure.error.reason}"
          )
      }
      .await
  }

  def deleteInES(record: ProcessedRecord, esIndex: String, fullyQualifiedId: Boolean = false): ProcessedRecord = {
    val recordId = if (fullyQualifiedId) {s"https://memobase.ch/record/${record.recordId}"} else {record.recordId}
    client
      .execute {
        logger.info(
          s"Deleting $recordId in Elasticsearch index $esIndex${if (record.dryRun) " (dry-run mode)"
          else ""}"
        )
        deleteById(esIndex, recordId)
      }
      .map {
        case success: RequestSuccess[_] if success.status < 400 =>
          logger.debug(
            s"Deletion of record $recordId in Elasticsearch index $esIndex successful${if (record.dryRun) " (dry-run mode)"
            else ""}"
          )
          record.setRecordStatus(
            ProcessingSuccess,
            "Record deletion in Elasticsearch index $esIndex successful"
          )
        case _: RequestSuccess[_] =>
          logger.debug(
            s"Deletion of record $recordId in Elasticsearch index $esIndex failed since record does not exist${if (record.dryRun) " (dry-run mode)" else ""}"
          )
          record.setRecordStatus(
            ProcessingIgnore,
            "Record does not exist in Elasticearch index $esIndex"
          )

        case failure: RequestFailure =>
          logger.warn(
            s"Deletion of record $recordId in Elasticsearch index $esIndex failed${if (record.dryRun) " (dry-run mode)"
            else ""}: ${failure.error.reason}"
          )
          record.setRecordStatus(
            ProcessingFatal,
            s"Record deletion in Elasticsearch index $esIndex failed${if (record.dryRun) " (dry-run mode)"
            else ""}: ${failure.error.reason}"
          )
      }
      .await
  }
}
