/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{ProcessResults, ProcessedRecord}
import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import scala.collection.JavaConverters._

trait MessageHandler {
  self: Logging with AppSettings =>

  lazy private val consumer = {
    val consumer = new KafkaConsumer[String, String](consumerProps)
    logger.debug(s"Subscribing to topic $inputTopic")
    consumer.subscribe(List(inputTopic).asJava)
    consumer
  }

  def poll: Iterable[ConsumerRecord[String, String]] = {
    val records = consumer.poll(Duration.ofMillis(10000)).asScala
    logger.debug(
      s"${records.size} new records fetched from Kafka topic $inputTopic"
    )
    records
  }

  def buildRecordProcessor(
                            recordId: String,
                            headers: Headers
                          ): ProcessedRecord =
    ProcessedRecord(
      recordId.replace("https://memobase.ch/record/", ""),
      None,
      List(),
      ProcessResults(),
      headers,
      headers.headers("dryRun").asScala.lastOption.flatMap(x => Some(x.value()(0).toInt == 1)).getOrElse(false)
    )

  def consumerClose(): Unit = {
    logger.debug("Closing Kafka consumer instance")
    consumer.close()
  }

}
