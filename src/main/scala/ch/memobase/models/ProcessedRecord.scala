/*
 * Record Deleter
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import org.apache.kafka.common.header.Headers

case class ProcessedRecord(
    recordId: String,
    thumbnailId: Option[String],
    binaries: List[String],
    processResults: ProcessResults,
    headers: Headers,
    dryRun: Boolean
) {
  def setRecordStatus(status: ProcessingStatus, msg: String): ProcessedRecord =
    this.copy(processResults =
      processResults.copy(record = Some((status, msg)))
    )

  def addBinary(path: String): ProcessedRecord =
    this.copy(binaries = binaries :+ path)

  def setDigitalObjectStatus(
      status: ProcessingStatus,
      msg: String
  ): ProcessedRecord =
    this.copy(processResults =
      processResults.copy(digitalObject = Some((status, msg)))
    )

  def setThumbnailStatus(
      status: ProcessingStatus,
      msg: String
  ): ProcessedRecord =
    this.copy(processResults =
      processResults.copy(thumbnail = Some((status, msg)))
    )

  def setBinariesStatus(
      status: ProcessingStatus,
      msg: String
  ): ProcessedRecord =
    this.copy(processResults =
      processResults.copy(binaries = Some((status, msg)))
    )
}
