# Record Deleter

Removes entries for records in indices and deletes related media files. 

The deletion process works per record and needs the respective ID as input. The following steps are then performed to remove a record:

1. Delete document in ES index
2. Lookup related media files in mediaserver DB
3. Delete entry in mediaserver DB
4. Delete related media files

## Configuration

In order to work as expected, the service needs a couple of environment variables set:

* `GROUP_ID`: Kafka consumer group id (see [Kafka documentation](https://kafka.apache.org/documentation/#consumerconfigs_group.id) for more information)
* `TOPIC_IN`: Name of Kafka topic where the deletion messages are coming from
* `TOPIC_PROCESS`: Name of Kafka topic where status reports are written to
* `MARIADB_TABLES`: Names of MariaDB tables where the respective rows should be deleted
* `MEDIA_FOLDER_ROOT_PATH`: The path where the media data is mounted inside the `Media Converter` container
